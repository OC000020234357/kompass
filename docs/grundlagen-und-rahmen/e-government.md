---
sidebar_position: 1
description: Einführung und Kontextualisierung des eGovG
---

# eGovernment Gesetz

Das Gesetz zur Förderung der elektronischen Verwaltung sowie zu weiteren Vorschriften (eGovernment Gestz) wurde 2013 vom deutschen Bundestag beschlossen und baut auf den vorhergehenden Reformvorhaben zur effizienteren Nutzung von Informationstechnik im Verwaltungskontext (z.B. Artikel 91c GG) auf. Ziel des Gesetzes ist es, sowohl einen vereinfachten elektronischen und sicheren Verwaltungszugang zu ermöglichen als auch digitalisierte Bearbeitungs- und Veraktungsprozesse zu fördern.

### Hintergrund

Das sog. EGovG ist nur eine Konsequenz aus dem, spätestens seit den 2000er Jahren verfolgten, E-Government-Leitbild. Der Gesellschaft für Informatik zufolge bedeutet dies eine umfassende Integration informationstechnischer Mittel für das politische und Verwaltungshandeln, darunter Entscheidungen, Beteiligung, Leistungs- und Prozessabwicklung [@Stemper_Egov]. Dieses Konzept schließt sowohl verwaltungsinterne bzw. zwischenbehördliche Handlungen als auch die Interaktion mit Bürger:innen und Unternehmen mit ein [@Stemper_Egov].  
Trotz der bis dato politischen Maßnahmen zur Modernisierung der öffentlichen Verwaltung, wurden den Bürger:innen und Unternehmen bis zum Beschluss des EGovG weiterhin nur Informationen sowie Formulare zum Download auf den Websites der Behörden angeboten, was unter anderem auf rechtliche Hindernisse zurückzuführen ist [@Herr_eGovG]. Diese sollten mit dem neuen Gesetz ausgeräumt werden. Zudem kann das Gesetz auch als eine Reaktion auf die wahrgenommene, steigende Distanz zwischen der öffentlichen Verwaltung und Bürger:innen bzw. Unternehmen gedeutet werden.

### Inhalt

„Die Kernpunkte des Gesetzes stellen sich folgendermaßen dar: Artikel 1 ist das E-Government-Gesetz. Die wesentlichen Regelungen sind:
-	Verpflichtung der Verwaltung zur Eröffnung eines elektronischen Kanals und zusätzlich der Bundesverwaltung zur Eröffnung eines De-Mail-Zugangs, 
-	Grundsätze der elektronischen Aktenführung und des ersetzenden Scannens, 
-	Erleichterung bei der Erbringung von elektronischen Nachweisen und der elektronischen Bezahlung in Verwaltungsverfahren, 
-	Erfüllung von Publikationspflichten durch elektronische Amts- und Verkündungsblätter, 
-	Verpflichtung zur Dokumentation und Analyse von Prozessen, 
-	Regelung zur Bereitstellung von maschinenlesbaren Datenbeständen durch die Verwaltung ("open data")“ [@VI_EgovG].

Damit wurden neben der qualifizierten elektronischen Signatur weitere Möglichkeiten für eine elektronische Alternative zur Schriftform erschlossen und anerkannt, welche eine sichere Identifizierung ermöglichen. Dies umfasst u.a. die de-Mail mit einer sicheren Anmeldefunktion sowie verwaltungsspezifische Online-Anwendungen, wie die eID-Funktion des Personalausweises [@VI_EgovG].

„Das Gesetz soll [...] über die föderalen Ebenen hinweg Wirkung entfalten und Bund, Ländern und Kommunen ermöglichen, einfachere, nutzerfreundlichere und effizientere elektronische Verwaltungsdienste anzubieten. Medienbruchfreie Prozesse vom Antrag bis zur Archivierung sollen möglich werden. Dabei sollen Anreize geschaffen werden, Prozesse entlang der Lebenslagen von Bürgerinnen und Bürgern sowie der Bedarfslagen von Unternehmen zu strukturieren und nutzerfreundliche, ebenenübergreifende Verwaltungsdienstleistungen „aus einer Hand“ anzubieten. Ebenso sollen Rechtsunsicherheiten beseitigt werden“ [@BMI_EgovG].

### Umsetzung und Kritik

Um die Handlungsfähigkeit öffentlicher Verwaltungen während der im Gesetz vorgesehenen, umfassenden Veränderung der Verwaltungsarbeit  zu bewahren, sollte die Umsetzung schrittweise erfolgen (für weitere Informationen siehe [@Herr_eGovG]). In der wissenschaftlichen Literatur wird dieses Gesetz durchgängig als ein notwendiger Schritt hin zu einer umfassenden Verwaltungsmodernisierung bewertet. Die teilweise unpräzisen Regelungen, z.B. die Forderung nach einem elektronischen Zugang, eröffnet den Kommunen „eine Vielfalt an Umsetzungsmöglichkeiten“ und kann potenziell zu uneinheitlichen, schwer kombinierbaren Einzellösungen führen [@Herr_eGovG].  
 
## Quellen
