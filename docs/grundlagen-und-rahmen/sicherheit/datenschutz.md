---
sidebar_position: 3
description: Datenschutzbestimmungen werden auf unterschiedlichen Ebenen beschlossen
---

# Datenschutz

Die Gesetzgebung zum Thema Datenschutz geschieht auf mehreren Ebenen. Innerhalb der Europäischen Union (EU) gilt die Datenschutz-Grundverordnung (DSGVO), auch General Data Protection Regulation (GDPR). Auf Bundesebene ist der Datenschutz präziser und zusätzlich durch das Bundesdatenschutzgesetz (BDSG) geregelt. Die Bundesländer können wiederum eigene Gesetze erlassen. Der Rahmen, bzw. Spielraum, wird durch das jeweils übergeordnete Gesetz festgelegt.

Auch in der [Single Digital Gateway (SDG)-Verordnung](/docs/grundlagen-und-rahmen/sicherheit/gesetze#sdg) werden wichtige Konzepte für den Datenschutz zusätzlich zu einem Verweis auf die DSGVO aufgegriffen, u.a. die Grundsätze der Datenminimierung, der Notwendigkeit, der Verhältnismäßigkeit und der Zweckbindung.


## Datenschutz-Grundverordnung (DSGVO)

Die Datenschutz-Grundverordnung (DSGVO) [@DSVGO] regelt für die gesamte EU den Rahmen, in dem sich nationale Datenschutzgesetze bewegen müssen. Sie legt fest, wie mit personenbezogenen Daten umzugehen ist, damit der Schutz der Grundrechte und Grundfreiheiten von natürlichen Personen bei der Verarbeitung gewährleistet ist. Die Verarbeitung muss dabei nicht automatisch geschehen, um in den Anwendungsbereich zu fallen. Auch wenn die Verarbeitung außerhalb der EU geschieht, kann die DSGVO noch gelten, z.B. wenn die betroffene Person sich in der EU befindet.


### Grundsätze

Die DSGVO  [@DSVGO] legt in Art. 5 zunächst Grundsätze fest, aus denen die konkreten Aussagen abgeleitet werden. Diese Grundsätze überschneiden sich teilweise mit denen aus der Informationssicherheit.

Die Daten müssen auf folgende Weise verarbeitet werden:
* rechtmäßig,
* nach Treu und Glauben und
* für die betroffene Person nachvollziehbar.

Bereits die Erhebung muss festgelegten, eindeutigen und legitimen Zwecken dienen. Jede Weiterverarbeitung muss mit diesen Zwecken vereinbar sein. Es gibt wenige Ausnahmen, wie z.B. für statistische Zwecke. Die Daten, die erhoben werden, müssen alle dem Zweck angemessen und dafür notwendig sein. Zusätzlich müssen sie bezüglich des Verarbeitungszwecks richtig sein, andernfalls gelöscht oder korrigiert werden.
Die Daten dürfen nur solange gespeichert werden, wie es für den Verarbeitungszweck erforderlich ist.

Die Verarbeitung von personenbezogenen Daten muss deren Sicherheit gewährleisten. Konkrete Bedrohungen sind unbefugte und unrechtmäßige Verarbeitung, Verlust, Zerstörung bzw. Schädigung. Als Sicherheitsziele werden Integrität und Vertraulichkeit explizit genannt. Um die Risiken, die sich aus den Bedrohungen ergeben, zu mitigieren, beschreibt die DSGVO in Art. 32 technische und organisatorische Maßnahmen, die wir im Folgenden beschreiben.

### Technische und organisatorische Maßnahmen

Zunächst klärt Art. 32  [@DSVGO], dass der Auswahl von Maßnahmen eine umfangreiche Risikoanalyse vorausgehen sollte. Die Art des Schadens ist hier aber nicht wie üblich monetär, sondern betrifft Rechte und Freiheiten natürlicher Personen. Es gibt also keine bestimmten vorgeschriebenen Maßnahmen, mit denen man "auf der sicheren Seite" ist. Die möglichen Maßnahmen selbst umfassen verschiedene Arten von Sicherheitskontrollen, wie etwa physische, softwaretechnische, oder auch vertragsrechtliche. Eine Kategorisierung von Sicherheitskontrollen findet sich bei den [Softwaretechnik-Aspekten](softwaretechnik.md#sicherheitskontrollen). Einige werden explizit genannt, unter anderem Pseudonymisierung und Verschlüsselung.

### Öffentliche Verwaltung

Für Behörden sieht die DSGVO besondere Rechte und Pflichten vor. Zum Beispiel müssen nach Art. 37 [@DSVGO] verarbeitende Behörden oder öffentliche Stellen zwingend einen Datenschutzbeauftragten benennen. Mehrere Behörden oder öffentliche Stellen dürfen allerdings unter bestimmten Bedingungen einen gemeinsamen Datenschutzbeauftragten benennen.

Eine weitere Rolle spielen Behörden bei der Umsetzung der DSGVO selbst. Nach Art. 51 müssen Mitgliedsstaaten jeweils eine oder mehrere Aufsichtsbehörden einsetzen. Diese Behörden haben umfangreiche Befugnisse, um Datenschutzprüfungen durchzuführen und Meldungen von Verstößen nachzugehen. Außerdem können sie in das Zertifizierungswesen eingreifen und Verbote aussprechen bzw. Geldbußen verhängen.

## Bundesdatenschutzgesetz (BDSG)

Das Bundesdatenschutzgesetz (BDSG) [@BDSG] regelt die Erhebung und Verarbeitung personenbezogener Daten für öffentliche und nicht-öffentliche Stellen. Es ist die Umsetzung des EU-Datenschutzrechts auf nationaler Ebene, "erbt" aber die DSGVO und beugt sich dieser in § 1.

Die Pflicht zum Einsatz einer Aufsichtsbehörde nach Art. 51 [@BDSG] löst die Bundesrepublik durch Einrichtung der oberen Bundesbehörde des Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI). Dieser erfüllt die Funktion der Aufsichtsbehörde und übernimmt die in Art. 57 [@BDSG] angegebenen Aufgaben. Der BfDI ist allerdings nicht allein verantwortlich, sondern delegiert seine Rechte und Pflichten gemäß der föderalen Struktur der Bundesrepublik an die Bundesländer.

Entgegen der Amtsbezeichnung ist der BfDI nicht der Datenschutzbeauftragte nach Art. 37 [@BDSG] für öffentliche Stellen auf Bundesebene. Im Gegenteil - der BfDI hat sogar einen eigenen Datenschutzbeauftragten. Gleichzeitig erfüllt der BfDI nach § 14 auch die beratenden Aufgaben, die nach Art. 37 [@BDSG] von einem Datenschutzbeauftragen zu leisten wären. Eine Besonderheit bietet das BDSG noch für die öffentliche Verwaltung: gemäß § 43 (3) werden "[g]egen Behörden und sonstige öffentliche Stellen [...] keine Geldbußen verhängt."


## Datenschutzgesetze der Länder am Beispiel von Thüringen
Das Thüringer Datenschutzgesetz (ThürDSG) [@ThuerDSG] benennt wiederum einen Landesbeauftragten für den Datenschutz, der auf Landesebene die Funktion der Aufsichtsbehörde wahrnimmt und damit die Vorschriften der DSGVO umsetzt. Die Aufgaben werden in § 6 aufgelistet und entsprechen denen in Art. 57  [@ThuerDSG].

Auch auf Landesebene ist der Beauftragte für den Datenschutz nicht der Datenschutzbeauftragte für die öffentlichen Stellen des Landes, die sich im Übrigen wie auf Bundes- und Unionsebene einen Datenschutzbeauftragten teilen können. Und in Thüringen gilt wie auf Bundesebene nach § 61 (4): "Gegen öffentliche Stellen nach § 2 Abs. 1 und 2 werden keine Geldbußen verhängt [...]." [@ThuerDSG].

## Quellen
