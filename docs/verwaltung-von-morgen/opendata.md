---
sidebar_position: 4
description: Einführung und Einordnung des Konzepts
---

# Open Data

Das Konzept Open (Government) Data beschreibt den Zugang zu und die Nutzung von Daten auf Basis von „offenen und diskriminierungsfreien Lizenzen“ [@BMI_OpenData]. Wie der Zusatz Government bereits suggeriert, handelt es sich um „staatliche (kommunale) Datenbestände“ [@WewerOpenData]. Dies gewinnt besonders aufgrund des wirtschaftlichen Potenzials von Daten als Rohstoff zunehmend Bedeutung.

### Begrifflichkeit

Der Begriff Open (Government) Data unterscheidet sich vom Open Government Ansatz folgendermaßen:. Während Open Data als ein Teil von Open Government gesehen werden kann, bedeutet Open Government im wesentlichen „die Öffnung der Verwaltungen und die Einbeziehung der Bürgerinnen und Bürger in die Gestaltung des Gemeinwohls“ und zwar mit Hilfe digitaler Kollaborationswerkzeuge zur Zusammenarbeit über Organisationsgrenzen hinweg und insbesondere die Co-Produktion von Verwaltungsleistungen mit Externen [@Krabina_OpenGovernment]. 

### Hintergrund

Das erste Open Data Konzept entstand bereits 1957/58 im Rahmen des sogenannten Geophysikalischen Jahres. 2007 formulierte die sog. Sebastopol Group, bestehend aus Tech-Firmen und politischen Aktivist:innen, acht Prinzipien für offene Daten, so dass diese kommerziell zur Herstellung technologischer Applikationen dienlich sind:
-	Vollständigkeit
-	Primärquelle
-	so schnell wie möglich
-	Zugänglichkeit
-	Maschinenlesbarkeit
-	Diskriminierungsfreiheit
-	nicht proprietär
-	Lizenzfreiheit [@WewerOpenData]

Diese wurden 2010 von der Sunlight Foundation um zwei weitere ergänzt: vollständige Gebührenfreiheit und nachhaltige Datenhaltung. Für seine erste Amtsperiode integrierte Barack Obama diese Prinzipien weitestgehend unter dem Leitbild Open Government in sein Programm [@WewerOpenData]. Seit 2013 wird Open Data auch im politischen Kontext zur Veröffentlichung von Verwaltungsdaten diskutiert. Zentral war dabei die von den G8-Staaten beschlossene Open Data Charta und der darauf aufbauende Nationale Aktionsplan (2014) von Deutschland [@BVA_OpenData]. Daraus ergaben sich die folgenden bis heute wichtigen Aspekte für die Umsetzung von Open Data:
- Freie Wiederverwendung,
- Ein Lizenzenkonzept, das eine freie Wiederverwendung unter datenschutzrechtlichen Bedingungen sichert (weitere Informationen siehe [@BVA_OpenData]),
- Maschinenlesbare Formate,
- Und die Pflege von Metadaten (Basisinformationen, wie Ablageort, Erstellungsdatum usw.) zur Qualitätssicherung [@BVA_OpenData].

Für das Erreichen von Open Data im bundesdeutschen Verwaltungskontext trat am 13. Juli 2017 eine Änderung des E-Government-Gesetzes in Kraft, welches um §12a erweitert wurde. Dieses erste Open-Data-Gesetz verpflichtete die Behörden der Bundesverwaltung dazu, ihre Rohdaten, sofern datenschutzrechtlich unbedenklich, zu veröffentlichen. Zudem legte dies zu erfüllende Kriterien für die veröffentlichten Daten fest. Darunter sind u.a. ein entgeltfreier Zugang, Maschinenlesbarkeit sowie rechtliche Gründe für/gegen die Veröffentlichung [@BVA_OpenData].

### Potenziale und Ziele

Dem Open Data Ansatz werden viele verschiedene Potenziale zugeschrieben. Erstens soll durch die lizenzfreie Nutzung von Daten die Innovationskraft der bundesdeutschen Wirtschaft gestärkt werden, da viele Unternehmen ihre Geschäftsmodelle auf Datenbasis aufbauen [@BVA_OpenData].

Zweitens habe Open Data ein großes demokratisierendes Potenzial, da Bürger:innen über frei zugängliche Daten ein besseres Verständnis von Politik und Verwaltung sowie einen Einblick in politisches Handeln bekommen können. Dies soll die Motivation und Initiative zu bürgerschaftlichem Engagement und Problemlösekompetenz steigern [@BVA_OpenData].

Drittens kann auch öffentliche Verwaltung selbst von Open Data profitieren. Beispielsweise indem externe Datenkompetenzen (Verstehen, Interpretieren, Nutzen) von Unternehmen und Organisationen für die Erfüllung öffentlicher Aufgaben genutzt werden. Ein weiteres Beispiel ist ein effizienterer zwischenbehördlicher Datenaustausch [@BVA_OpenData]. Zuletzt werden auch indirekt wirksame positive Entwicklungen in den verschiedenen Bereichen vermutet, so das Kompetenzzentrum Open Data des Bundesverwaltungsamtes. Zur Veranschaulichung, siehe den Open-Data-Kreislauf im [Handbuch für offene Verwaltungsdaten des CCOD (S.15)](https://www.bva.bund.de/SharedDocs/Downloads/DE/Behoerden/Beratung/Methoden/open_data_handbuch.pdf?__blob=publicationFile&v=8).

Entsprechend formuliert das BMI das Ziel von Open Data folgendermaßen: „Das Ziel ist daher der Aufbau eines Daten-Ökosystems, in dem Wirtschaft, Verwaltung und Zivilgesellschaft gegenseitig von einer guten Datenbasis und breiten Nutzungsmöglichkeiten profitieren können“ [@BMI_OpenData].

### Umsetzung

Die Wirksamkeit des 2017 erlassenen Open Data Gesetzes stellen u.a. Kubicek/Jarka infrage. Diese sehen darin lediglich Anforderungen an eine diskriminierungsfreie Bereitstellung von Daten formuliert [@Kubicek_OffeneDaten]. Neben dem Erlass wurden im Rahmen des Aktionsplans für die Open Government Partnership verschiedene Projekte in die Wege geleitet, darunter der Zugang zu Geodaten und zu wissenschaftlichen Veröffentlichungen. 2021 folgte das „Gesetz zur Änderung des E-Governmentgesetzes und Einführung eines Gesetzes für die Nutzung von Daten des öffentlichen Sektors“ (zweites Open Data Gesetz; [@WewerOpenData]). Damit wurden in erster Linie weitere Behörden zur Umsetzung von Open Data verpflichtet und eine Evaluation der Umsetzung in die Wege geleitet. Die Verantwortung für die Koordination der veröffentlichten Daten sowie für eine „möglichst effiziente Umsetzung“ liegt dabei beim Bundesministerium für Inneres, Bau und Heimat [@WewerOpenData].

Für eine konsequente Umsetzung von Open Data wurde bereits 2018 ein Kompetenzzentrum Open Data als unterstützende Institution gegründet. Dieses steht beratend den Behörden „bei der Identifikation, Aufbereitung und Veröffentlichung von geeigneten Daten“ zur Seite und „steht zugleich als unmittelbarer Kontakt für alle Prozesse zur Anbindung offener Verwaltungsdaten an das nationale Metadatenportal GovData zur Verfügung“ [@WewerOpenData]. [GovData](https://docs.fitko.de/resources/govdata/) (seit 2015 in Betrieb) soll dabei als zentrale Stelle für die Suche nach „offenen Daten der Verwaltung“ dienen [@WewerOpenData][@BVA_OpenData].

2021 folgte zudem die Veröffentlichung einer Bundesweiten Open Data Strategie, welche die folgenden drei Handlungsfelder beinhaltet:
-	„Verbesserung der Datenbereitstellung sowie Auf- und Ausbau leistungsfähiger und nachhaltiger Dateninfrastrukturen
-	Steigerung einer innovativen, gemeinwohlorientierten und verantwortungsvollen Datennutzung
-	Förderung von Datenkompetenzen und Etablierung einer Datenkultur in der Bundesverwaltung zur Erhöhung von Qualität und Nutzbarkeit bereitgestellter Daten“ [@BMI_OpenData].

### Kritik

In Ihrem Beitrag zu offenen Daten (Handbuch Digitalisierung in Staat und Verwaltung 2020) unterziehen Herbert Kubicek und Juliane Jarke das Vorhaben Open Data einer kritischen Reflektion. Als erstes deuten die vorherigen Ausführungen nicht auf den enormen Kostenaufwand hin, welcher mit der Veröffentlichung vorhandener Daten verbunden ist. Diese erstrecken sich von "aufwändige[n] rechtliche[n] Prüfungsprozesse[n]" über "technische Anpassungsprozesse", welche zunächst definiert werden müssen, hin zur Einbindung von kompetenten Angestellten, welche das genannte Vorhaben wiederum begleiten [@Kubicek_OffeneDaten]. Ein ausgewählter Handlungsleitfaden für einen behördeninternen Bereitstellungsprozess offener Daten untermauert die beschriebene Komplexität.

Weiteren Hindernissen begegnet man bei dem Versuch, die bereitgestellten Daten in ein übergeordnetes Plattformsystem zu integrieren. Aufgrund der Heterogenität und Eigenständigkeit der datenhaltenden Stellen und der Heterogenität der zu katalogisierenden Daten wird eine Harmonisierung der Regelungen von Metadaten zusätzlich erschwert [@Kubicek_OffeneDaten]. Kubicek/Jarke zufolge erfolgt dies in der Regel nur unter hohem politischen Druck, welcher, so die Autor:innen, bei der Umsetzung der Open Data Strategie ausblieb [@Kubicek_OffeneDaten].

Neben Fragen der Umsetzung wird Open (Government) Data in der wissenschaftlichen Literatur auch hinsichtlich der konzeptionellen Ausarbeitung und den darin enthaltenen Werten und Zielen infrage gestellt [@WewerOpenData]. Inwiefern die Potenziale von Open Data wirklich wirtschaftliche und gesellschaftliche Vorteile bringen, geschweige denn voll ausgeschöpft werden (können), kann bis dato nicht beantwortet werden. Dem Politikwissenschaftler Göttrick Wewer zufolge gibt es jedoch mehrere Mythen zu beleuchten: Zum einen sei ein Vorteil von Open Data aufgrund verschiedener technischer Barrieren nicht automatisch für jeden Menschen garantiert. Zum anderen muss eingängig reflektiert werden, welche Daten ohne Bedenken veröffentlicht werden können, zum Beispiel weil eventuell dadurch eine natürliche oder juristische Person zu Schaden kommt [@WewerOpenData]. Hierbei muss zudem hinterfragt werden, welche Daten für eine politisierte Gesellschaft relevant sind und nach welchen (unbewussten) Kriterien eine selektive Öffentlichmachung geschieht [@Schuppan_OpenData]. Auch Qualitätsstandards stellen eine Hürde für die einfache Freigabe jeglicher Daten da, sodass diese nicht ohne weiteres veröffentlicht werden können. Datennutzung bedeutet auch, die notwendigen Kompetenzen und Ressourcen zu haben, wodurch der demokratische Gedanke infrage gestellt wird. Entsprechend bedeutet Open Data nicht gleich ein offenes, auf Transparenz und Bürgerbeteiligung ausgerichtetes Verwaltungshandeln (für weitere Informationen zu Open Government siehe [@OpenGovernment_Lucke]). Zuletzt muss auch gefragt werden, wer diese Daten überhaupt nutzen möchte [@WewerOpenData].

Aufgrund der lückenhaften und geringen Datenbereitstellung konnte auch in der Praxis kein erkennbarer Effekt, sowohl gesellschaftlich als auch innerhalb der Verwaltung, festgestellt werden. Zudem wird kritisiert, dass die Zielsetzung anhand eines unreflektierten Transparenzbegriffs ausgerichtet wird, welcher die notwendigen Faktoren rezipierter erlebter Transparenz in der Gesellschaft außer Acht lässt [@Kubicek_OffeneDaten]. Angesichts der bisherigen Nutzung, beispielweise in Apps, welche Geodaten für verfügbare Parkplätze einsetzen, muss weiter gefragt werden, ob der Zugang via Open Data gegenüber der herkömmlichen Anfrage ernsthafte Vorteile mit sich bringt.


## Quellen
