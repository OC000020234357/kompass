---
sidebar_position: 1
sidebar_label: Antragslose Verfahren
description: das antragslose Verfahren ist die Idealvorstellung von digitaler Verwaltung.
---

# Antragslose Verfahren nach §35a LAO, Verfahrensordnung

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Gemäß der Typologie im folgenden Abschnitt zur Spürbarkeit der Verwaltungsinteraktion ist das antragslose Verfahren die letzte Stufe, d.h. die proaktive Verwaltung, bei der Zugangsgestaltung.
Als gutes Beispiel für eine solche proaktive Verwaltung wird häufig die antragslose Familienbeihilfe (Kindergeld) in Österreich genannt.
Hier werden bei Geburt eines Kindes die benötigten Daten durch das Krankenhaus aufgenommen und an alle zuständigen öffentlichen Stellen weitergeleitet, so dass für die Eltern kein weiterer administrativer Aufwand entsteht, um Kindergeld zu erhalten. 
