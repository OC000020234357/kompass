---
sidebar_position: 5
description: Je nach Datenkategorie gibt es unterschiedliche Datenhaltungsomponenten
---

# Schicht: Datenhaltung

Verwaltungleistungen, die etwa im Zuge der OZG-Umsetzung elektronisch abgebildet und betrieben werden, verfügen in der Regel nicht über eine klassische Datenhaltungskomponente. Die einer elektronischen Verwaltungsleistung zugrunde liegende Architektur besteht aus verteilten Systemen, die vom Bund und den Ländern gemeinsam betrieben werden. Aus dem Prozess eines digitalen Amtsbesuchs lassen sich grundsätzlich vier Datenkategorien ableiten:

* Kategorie 1: Identifizierungsdaten der antragsstellenden Person
* Kategorie 2: Antragsdaten
* Kategorie 3: Verfahrensdaten zu den Antragsdaten
* Kategorie 4: Daten des elektronischen Verwaltungsakts
* Kategorie 5: Behördliche Register

## Identifizierungsdaten der antragstellenden Person

In der Regel verbirgt sich hinter einer elektronischen Verwaltungsleistung der Antrag auf Erlass eines Verwaltungsaktes. Der:die Antragssteller:in hat sich gegenüber der Behörde zu identifizieren. Der:die Antragssteller:in kann über mehrere Identitäten verfügen, die verschiedene Vertrauensniveaus (gering, substantiell und hoch) erfüllen. Der:die Antragssteller:in kann je nach Vertrauensniveau aus verschiedenen Systemen zur Speicherung seiner Identitätsdaten wählen. Folgende Systeme halten Identifikationsdaten der Nutzer:innen vor:

* Nutzerkonto Bund (VN: niedrig, substantiell, hoch) (https://id.bund.de/de/eservice/konto)
* mit dem Nutzerkonto Bund interoperable Servicekonten der Länder (VN: niedrig, hoch) (bspw. https://servicekonto.thueringen.de/serviceaccount/)
* Elsterzertifikat (VN: substantiell)
* Elster-Unternehmenskonto
* Personalausweis mit Chip

## Antragsdaten

Zu jedem Antrag einer elektronischen Verwaltungsleistung, bspw. auf Erlass eines Verwaltungsaktes, sind spezifische Antragsdaten an die jeweiligen Behörde zu übermitteln. Hierzu zählen u.a. auch Nachweise, beispielsweise die Geburtsurkunde oder Einkommensbescheinigungen, die dem jeweiligen Antrag beizufügen sind. Antragsdaten werden in der Regel in einem elektronischen Formular erfasst und nach dem Absenden in einem standardisierten XML-Format (z.B. als Modul innerhalb eines XÖV-Standards oder die XDatenfelder in einem XFall-Container) der jeweiligen Behörde übermittelt. Eine Speicherung der Antragsdaten erfolgt dann innerhalb der Behörde, die aufgrund der sachlichen und örtlichen Zuständigkeit Verfahrensbeteiligter ist.

## Verfahrensdaten zu den Antragsdaten

Die Antragsdaten werden durch die Routingkomponenten Fit-Connect oder durch die DVDV-Infrastruktur in die für den Antrag sachlich und örtlich zuständige Behörde übermittelt. Innerhalb dieser Behörde werden die Antragsdaten zusammen mit weiteren Daten, die die Behörde bei der Antragsbearbeitung generiert, in sogenannten Fachverfahren gehalten. Die Datenhaltung erfolgt demnach innerhalb der Behörde bzw. bei einem für die Behörde arbeitenden IT-Dienstleister (Rechenzentrum). Fachverfahren sind spezielle Softwareprogramme mit Fachbezug, die für die Bearbeitung des jeweiligen Antrags zum Einsatz kommen (spezifisches Fachverfahren). Nicht jede Verwaltungsleistung wird mit einer dedizierten Software bearbeitet. Oftmals kommen auch generische Fachverfahren wie die eAkte zum Einsatz.

## Daten des elektronischen Verwaltungsakts

Im Ergebnis des von der antragsstellenden Person angestrengten Verwaltungsverfahrens entsteht ein begünstigender bzw. belastender Verwaltungsakt, der üblicherweise als Bescheid an die antragsstellende Person adressiert wird. Für solche Bescheide existiert eine Aufbewahrungspflicht innerhalb der Behörde. Eng verbunden mit der Aufbewahrungspflicht ist die Aufbewahrungsfrist, die je nach Leistungsart unterschiedlich sein kann. Ein solcher Bescheid kann entweder über das Postfach des Nutzerkontos (§9 OZG) oder gemäß §41 Abs. 2 VwVfG auch aus einem Cloud-Speicher abgerufen werden.

## Behördliche Register

Zukünftig sollen die zu den Antragsdaten gehörenden Nachweise wie Geburtsurkunde, Gewerberegisterauszug etc. nicht mehr durch die antragsstellende Person beigebracht werden, sondern aus vernetzten Registern abgerufen werden. Damit vereinfacht sich der Antragsprozess erheblich, weil Daten und Nachweise nicht immer wieder erneut für die Erbringung von Verwaltungsleistungen vorgelegt werden müssen. Vielmehr sollen staatliche Stellen, das Einverständnis der Bürgerinnen und Bürger vorausgesetzt, Daten und Nachweise, welche bereits vorliegen, selbst abrufen.

Die rechtliche Grundlage bildet das Registermodernisierungsgesetz (RegMoG), welches im April 2021 im Bundesgesetzblatt verkündet wurde. Art. 1 RegMoG beinhaltet das Identifikationsnummerngesetz IDNrG, welches voraussichtlich Ende 2023 in Kraft tritt. Die Voraussetzung für das Inkrafttreten liegt in der Bereitstellung des Fachverfahrens zum Identitätsdatenabruf (IDA) durch das BVA und der Bereitstellung des Datenschutzcockpits (DSC) durch die Freie Hansestadt Bremen. Nach aktueller Planung muss gemäß § 2 Abs. 1 IDNrG das IDNrG bis Ende 2028 umgesetzt werden.
