---
sidebar_position: 5
description: Komponenten für Feedback- und Nutzungsauswertung 
---

# Nutzeranalyse

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

An Dieser Stelle sollen die verschiedenen Komponenten zur Auswertung der Nutzer:innenfeedbacks und des Nutzungsverhaltens auf nationaler und europäischer Ebene beschrieben werden.

* Nationale Feedbackkomponente
* Nationale Statistikkomponente
* EU SDG Statistikkomponente
