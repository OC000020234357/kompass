---
sidebar_position: 3
description: Das elektronische Nachrichtenübermittlungssystem der EU
---

# eDelivery

## Allgemein
Das Europäische System eDelivery bietet eine elektronische Nachrichtenübermittlung, basierend auf OASIS AS4 [@eDeliveryGovernment]. Es erfüllt Sicherheitsanforderungen der eIDAS Verordnung. Die notwendigen Komponenten sind:

* Access Point
* Service Metadata Locator (SML)
* Service Metadata Publisher (SMP)

Für diese Komponenten steht jeweils Beispielsoftware zur Verfügung. Die Komponenten können aber auch selbst entwickelt werden. So entsteht kein Zwang auf Software von Dritthersteller:innen zurückgreifen zu müssen. Allerdings gibt es auch Anbieter:innen, die eDelivery als Dienst anbieten und die Komponenten für die öffentliche Verwaltung verwalten. Ebenso werden Workshops, Schulungen, ein Service Desk und eine Testplattform, ein Deplyoment Support und Software-Tools angeboten, die bei Selbsteinschätzung bezüglich der Bedarfsanalyse, Umsetzungsvariante und Kostenintensität helfen. 
Der eDelivery Service wird von Sektoren der öffentlichen Verwaltung gebraucht, wenn sie Dokumente sicher austauschen wollen. durch den Service kann die öffentliche Verwaltung über Ländergrenzen hinweg diese Dokumente austauschen. Somit wird der A2A Sektor für die öffentliche Verwaltung zugänglicher. Ebenso ist A2B oder B2A möglich, wie die PEPPOL Implementation zeigt. Auch können so A2C oder C2A etabliert werden, also die Kommunikation mit Bürger:innen.  

## Funktionsweise

Bei eDelivery [@eDeliveryDoku] reden die Systeme der Nutzer:innen (A - Administration, B - Bussiness oder C - Customer) nicht miteinander, sondern kommunizieren über Access Points miteinander. So ist die Kommunikation nicht nur sicher, sondern die Systeme können auch miteinander kommunizieren, wenn sie unabhänig voneinander entwickelt wurden. Somit wird eine domainneutrale Kommunikation ermöglicht.   
In ihren Kernkompenenten stehen vier UseCases: Nachrichtenaustausch mit Vertrauensbildung, eine dynamische Service-Lokation, eine Fähigkeitssuche und eine Backend-Integration. 
Der Nachrichtenaustausch bezieht sich auf den Austausch über das 4-Corner-Prinzip [@eDeliveryDoku]. Wie zuvor beschrieben, wird über Access Points miteinander kommuniziert. Während des Ausstauschs werden die Dokumente abgesichert durch Vertrauensbildung. Diese basiert auf digital ausgestellten Zertifikaten für die ausgetauschten Dateien. Damit kann sichergestellt werden, dass die Dateien von der jeweiligen Person stammen und es kann überprüft werden, ob sie beim Transport verfälscht worden sind. Dies kann erreicht werden, indem das Zertifikat mit dem Public Key des Empfängers erstellt wurde.
Die dynamische Service-Lokation wird benutzt, um den Standort des Empfängers zu lokalisieren. Diese Funktion übernimmt der SML, er teilt dem Sender mit, welcher SMP die Informationen über den Empfänger bereitstellt. Zu diesen Informationen zählt unter anderem die IP-Addresse des Accesspoints.
Die Fähigkeitssuche findet im SMP statt. Hier werden Informationen über Nachrichtentypen oder das Nachrichtenprotokoll hinterlassen, welche sich der Sender herunterladen kann, um sich so an den Empfänger anzupassen. 
Die Backend-Integration beschreibt, dass zwischem dem Access Point und dem Backend-System ein "Connector" platziert werden kann. Dieser Connector kann Verbindungen zwischen Systemen herstellen, Nachweise über Nachrichtenaustausch erstellen und diese überwachen. Hierdurch wird die Interoperabilität des Systems gesteigert sowie die Sicherheit und Nachweisbarkeit.  

## PEPPOL

Peppol ist eine Sammlung von Artefakten und Spezifikationen, welche grenzüberschreitende elektronische Beschaffung ermöglichen. Dabei können mit Hilfe der Artefakte und Spezifikationen Plattformen für den Informationsaustausch implementiert werden. Es basiert wie auch eDeliviery auf dem 4-Corner-Modell. Über Peppol Accesspoint können Nutzer:innen über das Peppol-Netzwerk Dokumente miteinander austauschen. Es funktioniert nach dem Prinzip, "Connect once, connect to all", also einmal zu allem verbunden. Mehr zu [Peppol](https://peppol.eu/what-is-peppol/) kann unter der offiziellen Website nachgelesen werden. [@PeppolWebsite]

## Quellen
