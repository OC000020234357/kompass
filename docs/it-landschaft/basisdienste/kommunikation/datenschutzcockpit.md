---
sidebar_position: 1.7
description: Transparenz und Nachvollziehbarkeit von Datenaustauschen
---

# Datenschutzcockpit
:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Im Rahmen des EfA-Prinzips entwickelt das Bundesland Bremen ein bundesweites Datenschutzcockpit. Das Ziel ist, Datenaustausche im Rahmen des Registermodernisierungsprozesses transparent und nachvollziehbar zu machen.
Das Bundesverwaltungsamt (BVA) assistiert dem Land Bremen bei der Entwicklung der Infrastruktur. Zusätzlich ermöglicht es den Zugriff auf die ID-Nummern über den Identitätsdatenabruf (IDA) [@BremenDatenschutzcockpitPressestelle].

Als zugrundeliegendes Übertragungsformat ist der [XÖV](/it-landschaft/basisdienste/datenuebermittlung/datenstandards/xoev.md)-Standard XDatenschutzcockpit (XDSC) festgelegt [@BremenDatenschutzcockpitSenator]. Das Datenschutzcockpit ist noch in der Anfangsphase. Ein erstes Pilotprojekt ist das Projekt "ELFE – Einfach Leistungen für Eltern" [@BremenDatenschutzcockpitPressestelle]. Dieses ist bis Ende 2022 geplant. Ab 2023 ist die überregionale Anbindung an den IDA geplant [@BremenDatenschutzcockpitSenator].

# Quellen
