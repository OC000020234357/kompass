---
sidebar_position: 1.5
description: Ein Informationsdienst über den Bearbeitungsstatus von Anträgen 
---

# Statusmonitor
:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

In der Bearbeitung von Anträgen gibt es verschiedene Stufen. Über den Statusmonitor soll künftig der Bearbeitungsstatus des Antrags einsehbar sein. Dies soll sowohl für die Kund:innen als auch für die öffentliche Verwaltung selbst gelten. Hierfür sollen verschiedene Portale genutzt werden können, unter anderem das Nutzerkonto-Bund.  [@FoederaleITLandschaft]

# Quellen
