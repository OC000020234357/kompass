---
sidebar_position: 3
description: E-Mailangebot des Bundes für Behörden, Unternehmen und Privatpersonen
---

# De-Mail

Das E-Mailangebot des Bundes, De-Mail wird von der Arbeitsgruppe De-Mail betrieben. Behörden, Privatpersonen und Unternehmen steht dieses zur Verfügung. De-Mail stellt die Funktionalität einer Email mit den folgenden zusätzlichen Sicherheitsfeatures bereit:

* zwei-Authentifizierungsniveaus
* Transportverschlüsselung
* Integrierter Viren-Scan
* Server ausschließlich in Deutschland
* Ende-zu-Ende-Verschlüsselung

Die De-Mail bietet Rechtssicherheit auf Grundlage des De-Mail-Gesetzes. Dies bedeutet, dass der Schriftverkehr ausschließlich mit eindeutig identifizierten Partner:innen erfolgt und der Mailversand nachweisbar ist. Ein wesentlicher technischer Sicherheitsfaktor ist, dass alle Server auf deutschem Boden stehen. Aktuell sind vier Unternehmen akkreditiert:

* Mentana-Claimsoft
* 1&1 De-Mail GmbH
* Deutsche Telekom Security GmbH
* Telekom Deutschland GmbH

Dabei ist jedoch zu beachten, dass der DE-Mail-Service der Deutsche Telekom Security GmbH, sowie der Telekom Deutschland GmbH am 31. August 2022 ausgelaufen ist. [@demail]
Es gab sehr viel Kritik unter anderem vom CCC, da wesentliche Sicherheitsstandards fehlten und sogar per Gesetzt herunter gesetzt wurden. Ein großer Kritikpunkt war zudem, dass eine kurzeitige Entschlüsselung der Mail auf dem Server per Gesetzt explizit nicht zum Sicherheitsproblem ernannt wurde.[@cccdemail]

# Quellen
