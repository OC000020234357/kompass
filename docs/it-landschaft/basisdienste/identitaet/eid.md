---
sidebar_position: 1
description: Diese Authentifizierungsfunktion kommuniziert mit dem Personalausweis-Chip
---

# eID
Die eID-Funktion entspricht dem höchsten Vertrauensniveau (siehe auch [Nutzerkonto Bund](nutzerkonto-bund)) bei der Authentifizierung [@BmihEidVerordnung]. 
Ein eID-Service regelt die Kommunikation mit dem Personalausweis-Chip sowie den Bezug aktueller Berechtigungszertifikate und Sperrlisten. Bei erfolgreicher Authentisierung übermittelt dieser Dienst die aus dem Ausweis-Chip gelesenen Daten der sich ausweisenden Person an den anfragenden Dienst. Die Kommunikation mit dem Anbieter verläuft über eine abgesicherte SSL-Verbindung.
Der eID-Service wird von verschiedenen Serviceanbietern betrieben. Eine Liste der Provider ist hier zu finden [@BmihAusweisportal].
Um Diensteanbieter zu werden, ist dieser [Anleitung](https://www.personalausweisportal.de/Webs/PA/DE/wirtschaft/diensteanbieter-werden/diensteanbieter-werden-node.html) zu folgen.

Ein Überblick über die eID-Infrastruktur ist unter [@BsiEidInfrastruktur] erhältlich.
Die Aufgaben eines eID-Servers sind unter [folgender Ressource](https://www.personalausweisportal.de/Webs/PA/DE/wirtschaft/technik/eID-server/eid-server-node.html) beschrieben.

### Sicherheit
Die Sicherheitsrichtlinien rund um die eID werden vom BSI in den [technischen Richtlinien](https://www.bsi.bund.de/DE/Themen/Oeffentliche-Verwaltung/Elektronische-Identitaeten/Technische-Richtlinien/technische-richtlinien_node.html) im Kontext von elektronischen Identitäten BSI-TR-3130 vorgeschrieben. Siehe dazu auch das Kapitel [Anforderungen aus den Gesetzen](/docs/grundlagen-und-rahmen/sicherheit/gesetze).
Die PKI des Personalausweises wird im Umfeld der Online-Ausweisfunktion über die [Anforderung CVCA-eID](https://www.bsi.bund.de/DE/Themen/Oeffentliche-Verwaltung/Elektronische-Identitaeten/Public-Key-Infrastrukturen/CVCA/Country-Verifying-Certificate-Authority-electronic-Identity/country-verifying-certificate-authority-electronic-identity_node.html) vom BSI spezifiziert.

### Implementierung
Es existiert zudem eine quelloffene Middleware, über die die EU-Mitgliedsstaaten mit dem eID-Service kommunizieren können, siehe dazu auch [eIDAS Nodes](/docs/it-landschaft/basisdienste/identitaet/eidas).
Eine Testumgebung für neue Funktionen ist (notwendig und) vorhanden [@BsiEidTestinfrastruktur]. Für den Test und die Entwicklung wird auch eine Test & Beta-PKI betrieben. Außerdem wird die Personalausweis-Funktionalität simuliert, um virtuelle Testausweise zu erstellen.
Die Test-Tools sind [öffentlich verfügbar](https://github.com/eID-Testbeds).

## Quellen
