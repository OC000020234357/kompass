---
sidebar_position: 3
description: Portalübergreifendes Nutzer:innenkonto für den Verwaltungszugang
---
# Nutzerkonto Bund

Das Ziel des Nutzerkonto-Bund ist es, Bürger:innen gegenüber allen behördlichen Portalen auszuweisen und die Inanspruchnahme von Onlinediensten mit einem einzigen Nutzerkonto zu ermöglichen. Dies entspricht den europäischen Vorgaben für die elektronische Identifizierung gegenüber digitalen Verwaltungsleistungen (eIDAS-VO). Die Anmeldung erfolgt über drei Vertrauensniveaus:

1. *Basis* - Passwort + Benutzername
2. *Substantiell* - Passwort + PKI-Verfahren
3. *Hoch* - eiD 

Die Basisregistrierung erfolgt über die Vergabe eines Benutzernamens und eines Passwortes. Die Anmeldung über das zweite Vertrauensniveau erfolgt mithilfe eines Softwarezertifikates mittels Elster, welches bereits die PK-Infrastruktur zur Verfügung stellt. Als letztes Vertrauensniveau existiert "hoch". Es ist ein hardwarebasiertes PKI-Verfahren mittels eID.

Onlineleistungen setzen die notwendigen Vertrauensniveaus voraus [@BmihNutzerkontoBundInfoflyer]. Je sensibler die Leistung ist, desto höher ist die Anforderung an das Vertrauensniveau. Die Nutzer:innen müssen sich zunächst registrieren, mit mindestens dem Basisniveau. Nachträglich können weitere Authentifizierungsmittel hinzugefügt werden. Über das NKB werden nach §8 OZG [@OZG] zulässige Informationen (Grunddaten) zur Weitergabe an Onlinedienste/Fachverfahren gespeichert. Dies können bei einer natürlichen Person Name, Vorname, Anschrift, Geburtsname, Geburtsland, Geburtsdatum, akademischer Grad und Staatsangehörigkeit sein. Die Weitergabe der Informationen erfolgt immer nur nach Freigabe durch den Nutzer. Die Erfassung der Grunddaten erfolgt entweder durch freiwillige Angabe oder bei der Nutzung der Authentifizierung durch das Auslesen der eID.

Nach der Anmeldung kann der Nutzer:

* Eigene Daten verwalten.
* Integriertes [Postfach](/it-landschaft/basisdienste/kommunikation/postfach.md) nutzen.
* Sich gegenüber Onlineleistungen identifizieren und deren Vertrauensniveaus erfüllen.


## Implementierung 
Relevante Informationen für die Implementierung sind hier aufgelistet:
- Eine Testumgebung ist vorhanden. Man kann sie [hier](https://keycloak-nutzerkonto.herokuapp.com/) erreichen.
- Die Integrationsumgebung von Onlinediensten ist [hier](https://int.id.bund.de) verfügbar [@BundIDWebsite]. Eine erfolgreiche Integration ist notwendig, bevor die Verbindung zur Produktionsumgebung erfolgen kann.
- Die Produktionsumgebung der Onlinedienste wird [hier](https://id.bund.de) betrieben.
- Die [Datenschutzerklärung](https://id.bund.de/de/eservice/konto/datenschutz) ist zu beachten.
- Beziehung zu anderen Basisdiensten [@BundIDWebsite].
  - [Mein Unternehmenskonto](https://mein-unternehmenskonto.de/), das einheitliche Unternehmenskonto (eUK) [@UnternehmenskontoWebsite]
  - ELSTER-PK-Infrastruktur.
  - Online-Ausweisfunktion mit AusweisApp2 bzw. eID.
  - Registrieren soll auch über eIDAS Nodes aus einem EU-Mitgliedsstaat möglich sein [@NutzerkontoBundInfobroschuere].

Die Integration in bestehende Webservices ist mit relativ geringem Aufwand verbunden, solange diese bereits ähnliche Standards wie OASIS SAML verwenden. Ist das nicht der Fall, ist eine Integration als nicht geringer Eingriff in die bisherige Funktionsweise der Web-Anwendung zu sehen.

![](/assets/IT-Landschaft/nutzerkonto-aufbau.png)

## Kommunikation
Der Kommunikation liegt "ein Ablauf in sieben Teilschritten" zugrunde, welcher "den international weitverbreiteten Standards der OASIS Group [folgt]" [@BundIDWebsite]:

1. Ohne Authentisierung nur Zugriff auf öffentliche Seiten.  
2. Authentication Request des Fachportals an Browser des Nutzers (OASIS SAMLv2, via HTTP-REDIRECT oder HTTP-POST).  
3. Weiterleitung des Authentication Request an den Identity Provider.  
4. Authentisierung (bidirektional).  
5. Signierte SAML-Repsonse.
6. Weiterleitung der SAML-Reponse und Überprüfung auf Authentizität und Integrität.   
7. Antwort des Anwendungsservers [@BSIeIDServer].  

Die Zugriffsrestriktionen sind ausschließlich Teil der Applikationslogik des Fachportals. Die Generierung von SAMLv2 OASIS-Token kann entweder innerhalb der Drittapplikation oder durch die Verwendung von Reverse-Proxys erfolgen. Das ITZ.Bund ist der Betreiber der SAML-basierten Identity-Infrastruktur des Nutzerkonto Bund und ist für die Zusicherung einer dauerhaften Erreichbarkeit zuständig.

![](/assets/IT-Landschaft/nutzerkonto_kommunikation.png)

## Betriebsvoraussetzungen
Die Erreichbarkeit des Identitätsproviders für die unterstützten Authentifizierungsmethoden wird durch das ITZBund sichergestellt. Die Betriebsvoraussetzungen sind wie folgt: Zuerst wird eine Kooperationsvereinbarung beim BMI eingereicht (Kontakt: bundID@bmi.bund.de). Dann erfolgt die Bereitstellung der SAML-Metadaten durch die Drittanwendung. Anschließend werden auch die SAML-Metadaten des Identity-Providers bereitgestellt.

## BundID-Dienst

 Die Pflicht zur Einbindung der BundID stammt zunächst aus der Einzelvereinbarung (EV), §2, Abs. 2:  
     
     Anbindung des Nutzerkontos Bund an den Online-Dienst ab „go live“; die Anbindung wird über den Integrationsprozess BMI-seitig unterstützt und erfolgt basierend auf der Schnittstellendokumentation für das Nutzerkonto Bund
   
 Ebenfalls ergibt sich eine Pflicht zur Einbindung der BundID aus den EfA-Mindestanforderungen. Dort heißt es unter Kriterium NK1:
 
     An den Online-Dienst MUSS ein interoperables Nutzerkonto angebunden sein. Bis alle Nutzerkonten interoperabel sind, MUSS mindestens das Nutzerkonto Bund für Bürgerinnen und Bürger bzw. das einheitliche Unternehmenskonto angebunden werden.
 
Derzeit sind noch nicht alle Landeskonten interoperabel. Eine belastbare Planung für die bundesweite Interoperabilität der Postfächer liegt derzeit ebenfalls nicht vor. Bei konkreten Rückfragen verfügt die FITKO über einen aktuellen Sachstand zur Interoperabilität der Konten und Postfächer.

In jedem Fall verpflichtet die EV allerdings zur direkten Anbindung der BundID/eUK, unabhängig vom Status der Interoperabilität. Zudem hat die BundID den Vorteil, dass sie die Vorgaben der europäischen Verordnung über elektronische Identifizierung und Vertrauensdienste (eIDAS-Verordnung) erfüllt.

### Anbindung
 
Die folgenden Punkte müssen im Rahmen eines Umsetzungsprojekts operativ behandelt werden. Die Plattformen zur Umsetzung von Online-Diensten müssen sich beim ITZBund registrieren.
Ggf. ist dafür eine Verwaltungsvereinbarung mit dem Bund erforderlich. Hierfür ist DV3 zuständig. Die Bearbeitungszeit dauert in der Regel zwei Wochen. Anschließend kann der folgende Prozess ablaufen:
1. Einbindung der BundID/eUK im Online Formular.  
2. Erhebung der Zustimmung zur elektronischen Bescheidung im Online-Dienst (Einzelzustimmung).  
3. Übernahme des Postkorb-Handles für das Postfach der BundID/eUK.  
4. Hinterlegung des Postkorb-Handles und der Einwilligung zur elektronischen Bescheidung im Datensatz und dem dort hinterlegten PDF (die benötigte Zeit beträgt höchstens einen Tag).  

### Zustellung
Die betroffenen Behörden, die für die Entscheidung zuständig sind, sind dann dafür verantwortlich, eine eventuell notwendige Entscheidung elektronisch zuzustellen. Hierzu ist in der Regel eine Zustellung in das Postfach nach § 9 OZG oder alternativ nach § 41 2 a VwVfG vorzunehmen. Die Länder sind für die Bestimmung der Voraussetzungen zuständig (im Prinzip sind diese Voraussetzungen bereits vorhanden). Die Rückfalloption für Behörden ist die bewährte elektronische Bescheidung gemäß § 41 2a VwVfG.

Die BundID bietet dabei auch eine Reihe von weiteren spannenden Features für die fachlichen Prozesse in den Behörden/Fachverfahren. Im SAML-Datensatz erhalten sie verschiedene Metadaten von dem:der Verwaltungskund:in zugespielt, unter anderem BBK/BBK2 zur eindeutigen Identifizierung des:der Verwaltungskund:in (BBK2 ist zu empfehlen).

Mit BBK/BBK2 kann man auch eine eineindeutige Zuordnung zum Fall erreichen. Dies ist wichtig für die Dubletten-Prüfung von Antragsdaten. Vom Postfach der BundID gibt es nach Zustellung einer Nachricht eine Quittung. Diese Quittung ist besonders wichtig. Es gilt diese in der Fallakte zu archivieren. Das Datum dieser Quittung ist das Bekanntmachungsdatum, welches im Fachverfahren zu erfassen ist. Weiterhin kann man eine Lesebestätigung seitens der bundID über den Abruf der Nachricht durch den Verwaltungskunden sich als Fachverfahren zusenden lassen, wenn Sie eine E-Mail für das Fachverfahren in der Message an das Postfach hinterlegt haben. Hier sind kreative Lösungen mit Mehrwert für den Verwaltungskunden und die Verwaltung denkbar.

### Technische Umsetzung
Umsetzungsprojekte müssen ihre Plattform am IDP der BundID registrieren (hierfür gilt es Metadaten bereitzustellen) und dann in ihren Formularen die BundID anbinden. Dies erfolgt über SAML. Weiterhin müssen Fachverfahren sich per SOAP an das Postfach anbinden und einen Rückkanal ermöglichen. Es gibt verschiedene Umgebungen je nach Produktreife, etwa Produktion, Vor-Produktion und eine Testumgebung. Die Kommunikation mit der BundID erfolgt immer über das Koppelnetz Bund-Länder. Darüber hinaus gibt es regelmäßige empfehlenswerte Techniker-Workshops. Diese Workshops finden jeden ersten Donnerstag im Monat ab 09:30 Uhr statt. Hier werden klassische Fragen zur Anbindung geklärt, sowie Prozesse und die Dokumentation zur Integration oft diskutiert. 

## Quellen
