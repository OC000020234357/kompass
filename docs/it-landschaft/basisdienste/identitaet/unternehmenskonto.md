---
sidebar_position: 4
description: Besonderheiten zur Identitätsfeststellung des Unternehmenskontos
---

# Unternehmenskonto
Bei dem Onlinedienst Elster wird eine Zertifizierungsdatei für die Nutzter erstellt, mit der man sich einloggen kann. Diese Registrierungsdatei kann auch für das [Unternehmenskonto](https://www.elster.de/elsterweb/infoseite/nezo) als Authentifizierung für Unternehmen genutzt werden. Interessant ist dabei, dass das Unternehmenskonto das Unternehmen identifiziert und eine zentrale Möglichkeit bietet, mit Ämtern zu kommunizieren. Die Beantragung sowie der Login ändern sich hierbei nicht. Wie bei Privatpersonen wird aus Sicherheitsgründen mit einer Zertifizierungsdatei gearbeitet. Es ist für andere Behörden möglich dieses Unternehmenskonto anzubinden. Ein Unternehmen kann bis zu 200 Zertifkate erstellen. Jedem dieser Zertifikate ist ein Postfach zugeteilt, somit können verschiedene Abteilungen in einem Unternehmen ihre je eigenen Postfächer haben. Allerdings müssen hier noch die Dienste der Länder angeschlossen werden. [@UnternehmenskontoWebsite]
Informationen zum Unternehmenskonto erhält man unter [Mein Unternehmenskonto](https://mein-unternehmenskonto.de/public/#Startseite) oder unter:

* https://service.mein-unternehmenskonto.de/

# Quellen
