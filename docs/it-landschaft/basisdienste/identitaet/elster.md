---
sidebar_position: 2
description: Das Steuerverwaltungsportal Elster arbeitet mit einer Zertifikatsdatei
---

# ELSTER
Elster bietet ein Online-Interface, mit dem die Steuerwaltung den Bürger:innen eine kostenlose Software zur Übermittlung ihrer Steuerdaten zur Verfügung stellt. Die Onlineformulare werden anschließend über einen Transportdienst direkt an die Finanzämter übermittelt. Die Formulare müssen nicht zusätzlich ausgedruckt werden.
Für die Leistung muss sich der:die Bürger:in allerdings authentifizieren. Dies erfolgt durch eine Zertifikatsdatei. Zur Aktivierung des Kontos stellt das zuständige Finanzamt der betreffenden Person postalisch einen Aktivierungs-Code und zusätzlich eine Aktivierungs-ID per Email zu. Durch die Onlineeingabe der Aktivierungsdaten kann der:die Nutzer:in anschließend eine Zertifikatsdatei herunterladen. Der Login erfolgt dann mittels der Aktivierungdatei, sodass weder ein Accountname oder die Angabe der Email-Adresse notwendig sind. Da für den Zugang die genannte Zertifikatsdatei benötigt wird, ist dieses Verfahren besonders gut vor Angriffen geschützt.
Nähere Informationen findet man in der [Registrierungshilfe](https://mf.sachsen-anhalt.de/steuern/elster/anleitungenhilfen/#c255494). [@elster]

# Quellen 
