---
sidebar_position: 7
description: Ein Verfahren zur Überprüfung der Sicherheit und Zuverlässigkeit von Personen
---

# OSiP (Online-Sicherheitsüberprüfung)
 
Die OSiP ist ein Verfahren zur Überprüfung der Sicherheit und Zuverlässigkeit von Personen, z. B. im Bereich der Flug- und Hafensicherheit oder von Atomkraftanlagen. Auch bei anderen Anträgen, bei denen die Zuverlässigkeit einer Person nach SiG kontrolliert werden soll, kommt die OSiP zum Einsatz. Sie ermöglicht nicht nur den Behörden einen umfassenden Informationsaustausch, sondern kann auch von Unternehmen genutzt werden. Zu diesen Unternehmen gehören unter anderem die Deutsche Lufthansa, Eurowings, UPS und FedEx [@OSiPFlyer]. 

OSiP ist ein Projekt, das ursprünglich im Rahmen des EfA-Prinzips von Nordrhein-Westfalen entwickelt wurde. Ab dem 1. Januar 2022 wurde OSiP von der FITKO übernommen (Stand 09/2022) [@NRWOSIPWebsite] und somit durch Haushaltsmittel des IT-Planungsrates finanziert [@NRWOSIPWebsite]. Dabei wird das OSiP Projekt zu einem OSiP-Produkt ausgebaut [@ItPlanungsratOSiPFinanzen]. Dies wird es den verschiedenen Bundesländern ermöglichen, OSiP uneingeschränkt einzusetzen [@NRWOSIPWebsite].

# OSiP-Komponenten
OSiP besteht aus verschiedenen Komponenten, und zwar dem OSiP-Kern und 4 OSiP-Clients: der Erkenntnisstellenclient, der Back-Office-Client, der Front-Office-Client und der Front-Office-Verwaltungs-Client. Der "OSiP-Kern" ist die "Datendrehscheibe". Er bietet die Möglichkeit, Anträge entgegenzunehmen, über regelbasierte Anfragen an Erkenntnisstellen weiterzuleiten, Erkenntnisrückmeldungen entgegenzunehmen und dann an Fachbehörden weiterzuleiten [@ItPlanungsratOSiPFinanzen].  
OSiP basiert auf XPS3, einer Infrastruktur des BKA, und ist modelliert auf XPolizei-konformen Daten [@NRWOSIPWebsite]. Der XÖV-Standard XZSÜ (Zuverlässigkeits- und. Sicherheitsüberprüfung) soll diese allerdings ablösen [@ItPlanungsratOSiPFinanzen]. Die OSIP-Clients sind in PHP realisiert, daher sind sie mithilfe eines Web-Browsers einfach zu erreichen und es ist keine Installation auf einer Workstation erforderlich. Allerdings sollen die neuen Client-Komponenten in Zukunft mit Java entwickelt werden, die die PHP Clients mit vereinheitlichten Datenstrukturen und den damit verbundenen Prozessabläufen ablösen sollen [@ItPlanungsratOSiPFinanzen].

![](/assets/IT-Landschaft/osip_komponenten.png)
 
# OSiP-Kommunikation
Die nachgelagerten Fachverfahren der Behörden sind mittels Webservices per SOAP und XML direkt an den OSiP-Kern angebunden [@NRWOSIPWebsite]. Unternehmen können allerdings direkt den FO-Client als Weboberfläche nutzen [@NRWOSIPWebsite]. OSiP wird überwiegend in gesicherten Verwaltungsnetzen betrieben [@NRWOSIPWebsite].

![](/assets/IT-Landschaft/osip_kommunikation.png)

# Quellen
