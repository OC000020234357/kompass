---
sidebar_position: 2
description: Beschreibung, Einordnung und Beispiele zum Thema Bundes- und Länderportale
---

# Bundesportal und Länderportale

Die Umsetzung des deutschen Portalverbunds macht die heterogene Landschaft der Bundes-, Länder- und Kommunalportale sowie die dazugehörigen Fachportale deutlich sichtbar. Zudem betont sie die Notwendigkeit gemeinsamer Standards und gleichzeitig die Schwierigkeit diese zu etablieren.

### Herausforderungen

Benjamin Stiebel (2021) zeigt in seinem Artikel zu den vom Bund festgelegten Sicherheitsregeln für den Portalverbund, dass die Verknüpfung einer Vielzahl verschiedener Portale und somit der unterschiedlichen Basisdienste, Anwendungen und Schnittstellen ein gewisses Sicherheitsrisiko mit sich bringt. Obwohl bundesweite Sicherheitsstandards zur notwendigen Risikominimierung beitragen, müssen die Auswirkungen dieser auf den Betrieb kommunaler Verwaltungen kritisch betrachtet werden. Da die angebotenen Leistungen direkt mit den Fachverfahren kommunaler Behörden verbunden sind, ist erstens eine Abgrenzung zwischen IT-Sicherheitsstandards von Portalverbund einerseits und der jeweiligen Organisation andererseits kaum durchführbar. Zweitens sind diese Anforderungen ohne zusätzliche personelle wie finanzielle Ressourcen nicht umsetzbar. Entsprechend muss eine Lösung, mit der alle leben können und welche dennoch die notwendigen Vorkehrungen erfüllt, erarbeitet werden [@Stiebel_portalverbund].

Doch nicht nur das Thema Sicherheit erschwert die Verknüpfung der vielen verschiedenen Portale. Da viele Verwaltungseinheiten bereits über ein eigenes Onlineportal verfügen, müssen diese sowie darin integrierte Komponenten hinsichtlich digitalisierter Leistungen erst einmal an vorgegebene Standards angepasst werden. Dies bedeutet wiederum einen großen Ressourceneinsatz für eine erfolgreiche Verknüpfung. Neben der technischen Veränderung elektronischer Dienstleistungen müssen nicht zuletzt auch die nur analog verfügbaren schnellstmöglich digitalisiert werden, sodass aus einem bundesweiten Flickenteppich ein attraktives Online-Angebot à la One-Stop-Shop Realität wird. Intuitive Handhabung, Barrierefreiheit und Nutzerfreundlichkeit sind auch hier notwenige Prinzipien für die Umsetzung [@Martini_Bürgerkto].

Hier geraten demnach erstens bundesweite Standardisierungsbestrebungen und die kommunale Selbstverwaltung in Konflikt. Zweitens wird sichtbar, dass ein unterstützendes Handeln von Seiten der Länder gegenüber den Kommunen zwingend notwendig ist [@Martini_Bürgerkto].

### Bundesportal

Das Bundesportal stellt die zentrale Sammel- und Zugangsstelle zu den gebündelten Leistungen und Informationen im Rahmen des föderalen Portalverbunds. Da sich dies derzeit noch im Aufbau befindet, beschränkt sich das Angebot auf Informationen zu den jeweiligen Verwaltungsleistungen, vereinzelte Onlinedienstleistungen sowie eine Liste aktueller Stellenangebote und Ausschreibungen des öffentlichen Dienstes [@BMI_bundesportal].

### [GovData](https://docs.fitko.de/resources/govdata/)

Das Portal GovData ist ein zentraler Zugangspunkt zu Verwaltungsdaten von Bund, Ländern und Kommunen. Dem Prinzip OpenData folgend sollen die Daten für alle Bürger:innen einfach zugänglich gemacht und zur Weiterverwendung bereitgestellt werden [@GovData_portal].
Aufgrund der Vielzahl der Portale empfiehlt sich eine Online-Recherche, welche sich spezifisch an den Bedürfnissen, dem Interesse, einer bestimmten Region oder einer konkreten Fragestellung orientiert. Über Metadaten erfahren Nutzer:innen genauere Informationen zu den jeweiligen Daten und können mit Hilfe eines Links auf den Ablageort zugreifen. Zur Qualitätssicherung der Metadaten kommt zudem ein Standard (DCAT-AP.de; für weitere Informationen dazu siehe [DCAT_AP](https://www.dcat-ap.de/)) dafür zu tragen. Dieser sichert die Weiternutzbarkeit der Daten sowie die Interoperabilität mit dem „übergreifenden Open Data Portal der EU, dem European Data Portal“ [@BMI_OpenData].

### [Geodatenkatalog der Geodateninfrastruktur Deutschland (GDI-DE)](https://docs.fitko.de/resources/geodatenkatalog/)

### [KoliBri](https://docs.fitko.de/resources/kolibri/)

Komponenten-Bibliothek für die Barrierefreiheit.

### [JoinUp](https://docs.fitko.de/resources/joinup/)

Eine kollaborative Plattform der Europäischen Kommission zur Unterstützung von E-Government-Fachleuten.

### YourEurope

Das Informationsportal YourEurope wurde 2005 im Rahmen der EU-DLR aufgebaut. Dort finden Bürger:innen und Unternehmen nationale Vorschriften für den Handel oder die Niederlassung in einem EU-Mitgliedsstaat nutzerfreundlich aufbereitet [@BVA_YE].

### Landesportale

Die technische Umsetzung und Betreibung eines Serviceportals zur Umsetzung der OZG-Anforderungen stellen ein Aufgabenspektrum dar, das durch eine einzelne Behörde nicht zu bewältigen ist. Es obliegt daher den Ländern, für die eigenen und nachgeordneten Kommunalbehörden Basisdienste für die Aufgabenerfüllung zur Verfügung zu stellen. Dafür gibt es verschiedene Ansätze. Erfolgversprechend erscheinen gegenwärtig die Bündelungen dieser Dienste in Landesportalen unter Nutzung der Vorteile gemeinsam mit anderen Ländern genutzter Basissoftware und deren Betrieb.

Beispiel dieser Zusammenarbeit ist das Thüringer Antragsportal ThAVEL, ein landesspezifisches Antragsportal, aufbauend auf dem Produkt GovOS der Firma FJD aus Kirchheim bei München. Das App-Konzept dieses Verfahrens ermöglicht länderspezifische und individuelle Leistungserweiterungen ohne Änderungen des Grundsystems und kann somit einfach für weitere Länder konfiguriert werden. Neben Thüringen nutzen Bayern und Niedersachsen dieses System.

### Thüringer Antragsportal ThAVEL

Das [Thüringer Antragssystem für Verwaltungsleistungen (ThAVEL)](https://thavelp.thueringen.de/thavelp/portal/desktop/0/login) ist die Landesplattform zur elektronischen Bearbeitung von Anträgen und anderen amtlichen Verwaltungsvorgängen im Rahmen der OZG-Vorgaben. Es basiert auf dem Produkt GovOS der Firma FJD und wird unter der Federführung des Thüringer Finanzministeriums entwickelt und auf der zentralen IT-Infrastruktur des Freistaates Thüringen betrieben. 

Der Freistaat Thüringen hat das ThAVEL-System für den gesamten Verwaltungsbereich sowie tangierte Institutionen wie Kammern und Eigenbetriebe des Freistaats Thüringen lizensiert und bietet landesweite technische Unterstützung.
ThAVEL leistet zwei grundsätzliche Aufgaben. Als Kommunikations- und Transaktionsplattform bietet es eine leicht zu bedienende Oberfläche für Online-Antragstellungen der Bürger:innen bei ihrer Verwaltung und für diese selbst eine elektronische Plattform, mit der die Anträge medienbruchfrei entgegengenommen und bearbeitet werden können.

Die vom Freistaat Thüringen kostenfrei zur Verfügung gestellten Basisdienste sind oder werden in ThAVEL integriert. Die über die Plattform nutzbaren Leistungen („Apps“) können über die bundesweiten Zuständigkeitsfinder erreicht werden. Dazu bietet ThAVEL eine Schnittstelle, mit der sich die zuständigen Stellen im Thüringer Zuständigkeitsfinder mit „ihren“ ThAVEL-Leistungen verknüpfen und in der Folge auch von allen anderen Zuständigkeitsfindern angesprochen werden können. Speziell generierte Zugangslinks sind auch in das bestehende Verwaltungs-Online-Angebot integrierbar. Nutzer:innenauthentifizierungen können unter Einbindung des Thüringer Servicekontos mit verschiedenen Authentifizierungslevels erfolgen. 

Die Paymentanbindung ePayBL erfolgt in ThAVEL einheitlich für alle Apps einmalig, es müssen nur die jeweiligen vorgangsspezifischen Parameter in der jeweiligen App konfiguriert werden.

Ebenfalls Bestandteil des Leistungspakets ist die Übertragung der Antragsdaten an die Zustellpunkte der zuständigen Stellen sowie die Auswahl und Konfigurierbarkeit der verschiedenen Übertragungswege.

Zentrale Komponente von ThAVEL ist die Bereitstellung der Antragsdokumente, Hilfetexte und zusätzliche Antragserläuterungen, eine automatische Zuständigkeitserklärung und eine Dokumentensuche für die Nutzer:innen. Die entsprechenden Leistungen dieser Antragsstrecken werden in einem App-Katalog vom Freistaat Thüringen nach vorheriger Prüfung und Freigabe zur Verfügung gestellt und können von den jeweiligen Behörden genutzt werden. Zudem können Verwaltungen Antragspakete aus bestehenden Apps adaptieren oder passgenaue Apps für besondere Aufgaben eigenständig zusammenstellen.

Vorteil der vom Freistaat beauftragten und freigegebenen Leistungs-Apps sind nicht nur die gesetzeskonformen und verwaltungseinheitlichen Antragsstrecken, sondern auch die OZG-konformen Übertragungsformate der Daten („XFall“), die eine medienbruchfreie Weiterverarbeitung über standardisierte Schnittstellen zu Fach- und Vorgangsbearbeitungssystemen ermöglicht.

Bei ThAVEL handelt es sich um ein ambitioniertes Projekt, das sowohl konzeptionell als auch technisch optimiert werden muss. Unter den gegenwärtigen Kritikpunkten sind folgende hervorzuheben:

Die OZG-Leistungsbündel in Apps zu gießen, ist ein aufwändiges und in der Gesamtheit langwieriges Unterfangen. Die Entscheidung und Beauftragung der vorrangig umzusetzenden Apps erfolgt durch Institutionen des Thüringer Finanzministeriums, die entsprechende Prioritäten setzen müssen, wie zum Beispiel bei den umzusetzenden Booster-Leistungen, die sich nicht immer mit den Fahrplänen der einzelnen Verwaltungen decken. Dies zeigt sich auch bei der Abwägung, Leistungs-Apps da zurückzustellen, wo aus Sicht des Freistaates besser EfA-Leistungen genutzt werden sollten. Letztendlich gibt es zu wenig Ressourcen oder zu aufwändige Freigabeprozesse, statt dem gegenwärtigen Bruchteil verfügbarer Verwaltungsleitungen rechtzeitig ein breites Spektrum an digitalen Antragsverfahren zur Verfügung zu stellen. 

Entgegenzuhalten ist allerdings, dass Verwaltungsmodernisierung immer Revision bestehender Prozesse bedeutet und dementsprechend aufwändig ist. Die ambitionierten gesetzlichen quantitativen Vorgaben müssen dementsprechend hinter den qualitativen Anforderungen zurückstehen.

Erheblich erhöhen ließe sich die Anzahl der digital erreichbaren Leistungen, wenn Verwaltungen auf regional besondere, kurzfristig erforderliche oder besonders nachgefragte Leistungsangebote aus dem eigenen Wirkungskreis mit einer selbst konfigurierten Antragsstrecke aus dem ThAVEL-Baukasten reagieren könnten. Dieser sogenannte universelle Onlineantrag steht trotz Forderungen der Verwaltungen und mehrfacher Ankündigung der Integration in einen AppBuilder nicht zur Verfügung. Sicherlich gilt es auch hierbei, einem „Wildwuchs“ von Antragsstrecken an den offiziellen, einheitlichen und qualitätsgesicherten Leistungs-Apps vorbei zu begegnen, doch sollte dies nicht diese derartigen Einschränkungen zur Folge haben.

Anforderungen an das Qualitätsmanagement gibt es auch an das ThAVEL-System selbst. Eine unzulängliche Nutzer:innenverwaltung lässt momentan durch fehlende Subrechte eine unkoordinierte, kaum kontrollierbare und nicht nachvollziehbare Leistungserstellung und -zuordnung zu, auch über Verwaltungsgrenzen hinweg. Hier muss unbedingt nachgebessert werden.

# Quellen
