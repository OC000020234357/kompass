---
sidebar_position: 3
description: Übersichtsdiagramm zum Ablauf eines EFA-Antragsverfahrens
---

# Komponenten und Interaktionen des OZG-Antragsverfahrens 
Anhand des im Folgenden verwendeten Sequenzdiagramms wird ein idealtypischer OZG-Prozess am Beispiel eines EfA-Antragsverfahrens von der Suche nach einer Verwaltungsleistung bis zum Transport des gestellten Antrags zur zuständigen Fachbehörde beschrieben. Endpunkt ist die möglichst medienbruchfreie Übernahme der Antrags- und Bezahldaten in die entsprechenden Bearbeitungssysteme der zuständigen Fachbehörde und die Rückübermittlung der Ergebnisse.

Die besondere Herausforderung an einen Online-Dienst, der dem Konzept EfA folgt, besteht darin, dass er bundesweit allen Gebietskörperschaften zur Mitnutzung zur Verfügung steht. Dies hat zur Folge, dass mehrere, unterschiedliche landes- und satzungsrechtliche Vorgaben beachtet und in die Antragsbearbeitung integriert werden müssen. Im Gegensatz dazu, agieren örtlich bzw. für eine spezifische Leistungsart vorgesehene Online-Portale mit fest verknüpften und dort gepflegten Behördeninformationen. 

Diese Parametrisierung der dazu notwendigen Dienste beinhaltet variable Werte des Online-Dienstes, Kommunikationsparameter für das Antragsrouting, Parameter für die Adressierung von Bezahldiensten sowie Parameter zur Steuerung des Aussehens der Online-Dienste und unterscheidet sich dahingehend von den diesbezüglich etwas "einfacheren" Prozessen hintern einem Online-Portal mit fest eingetragenen Parametern.

Um dies in seiner Komplexität darzustellen, wurde sich für das EfA-Verfahren entschieden, um alle der Komponenten und Parameteraufrufe des OZG-Antragsprozesses beschreiben zu können. Die jeweiligen Teilschritte werden in den dazugehörigen Teilprozessen näher erläutert.

Grundlage für das erstellte BPMN-Modell ist das Dokument "Orientierung Parametrisierung" des Föderalen IT-Architekturboards (Steuerungsgremium des IT-Planungsrates) [@FITKO_Parametrisierung]


## Prozessmodell Parametrisierung Komponenten OZG Verfahren aus EfA-Sicht
- [Prozessmodell - Link zur picture Plattform](https://www198.prozessplattform.de/jena/s/ozg_parametrisierung)
- [Prozessmodell - Download PDF](/assets/IT-Landschaft/OZG-Antragsverfahren__EfA__2022-10-18_110409.pdf)


# Quellen
