---
sidebar_position: 1
description: kurze Kontextualisierung der föderalen IT-Landschaft
---

# Allgemeines

Dieses Kapitel folgt der Struktur des föderalen IT-Architektur-Managements vgl. [IT-Planungsrat: "die föderale IT-Landschaft"](https://www.fitko.de/fileadmin/fitko/foederale-koordination/gremienarbeit/Foederales_IT-Architekturboard/20220610_Foederale_IT-Landschaft_Poster_v1.01.pdf).

### Hintergrund

Die Heterogenität der bundesdeutschen Digitalisierung zeigt sich auch in der technischen Umsetzung. Im Laufe der Zeit entstand ein „Flickenteppich“ mit einer Vielzahl dezentraler, eigenständiger und zum Teil proprietärer IT-Systeme und Fachverfahren in den Behörden auf Bundes-, Länder und kommunaler Ebene. Grund dafür waren fehlende zentrale Lösungsanforderungen und -ansätze sowie nicht-vorhandene Referenzmodelle und Rahmenvorgaben, zurückzuführen auf das bestehende Verständnis von geltenden Verwaltungsprinzipien. Insofern sind die Bemühungen der FITKO, gemeinsame und verbindende Basiskomponenten zu schaffen, die auch genutzt werden, sehr wichtig. Im Ergebnis soll der bestehende Flickenteppich mit Hilfe eines föderalen IT-Architektur-Managements vernetzt werden [@ITPR_dok:ITland].

### Föderales IT-Architektur-Management

Das Konzept des föderalen IT-Architektur-Managements soll im Folgenden analog zum klassischen Architektur-Management kurz zum besseren Verständnis dargestellt werden.

Auf der ersten Ebene befinden sich alle benötigten einzelnen Komponenten eines IT-Systems mit deren spezifischen Funktionalitäten. Analog stellen diese die verschiedenen Räume oder Elemente eines Raums dar.

Auf der nächsten Ebene werden diese Komponenten zu einem IT-System zusammengefügt, wodurch sie eine gemeinsame Funktionalität bilden, äquivalent zu einem Gebäude, in dem verschiedene Räume, wie ein Bad und eine Küche, die Funktionen zur Bedürfnisbefriedigung als Lebensgrundlage bündeln.

Dieses System wird dann auf der dritten Ebene in eine Systemlandschaft eingebettet, sodass es mit anderen Systemen interagieren kann, analog zu einer integrierten Stadtplanung. Das IT-Architektur-Management bearbeitet demnach die Frage, wie die einzelnen Systeme und Komponenten, entsprechend der beschriebenen IT-Landschaft und die dazugehörigen Organisationen vernetzt und gemeinsam genutzt werden können [@foederalesITArchitekturMan].

# Quellen
