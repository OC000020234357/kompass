---
sidebar_position: 3
---

# Föderale Informationsbebauung
Unter der föderalen Informationsbebauung sind alle im föderalen Kontext zum Einsatz kommenden Informationsobjekte zu verstehen. In der öffentlichen Verwaltung Deutschlands gibt es dabei neben der föderalen Aufteilung in Bund und Länder auch eine Verteilung von Verantwortlichkeiten auf Fachressorts (z.B. Inneres, Bau, Wirtschaft, Geo-Informationen usw.), in deren Kontext Informationsobjekte definiert und verwendet werden. In der Informationsbebauung wird dieser Verteilungsaspekt mit Fachdomänen abgebildet.

Informationsobjekte können abstrakte Geschäftsobjekte sein, die eine fachliche Semantik repräsentieren - beispielsweise ein Unternehmen. Für den Austausch über Schnittstellen werden dann konkrete Datenstrukturen definiert. Diese stellen ebenso ein Informationsobjekt dar, das in seiner Eigenschaft aber das abstrakte Geschäftsobjekt für einen technischen Einsatz implementiert.

In der Erarbeitung dieser idealerweise standardisierten Datenstrukturen für die Erstellung von Schnittstellenspezifikationen kommen unterschiedliche Frameworks (z.B. FIM Datenfelder, XÖV-Rahmenwerk) - in diversen Fällen auch gleichzeitig - zum Einsatz. Dies kann dann aber bereits auf semantischer Ebene trotz des Bestrebens zur Standardisierung zu abweichenden Strukturen führen - aufgrund unterschiedlicher organisatorischer und technischer Verantwortung.

Darüber hinaus gibt es für die Datenvorhaltung in Deutschland verschiedene zentrale und dezentrale Register (z.B. Melderegister, Handelsregister). Deren Strukturen können wiederum durch andere Organisationen oder Gremien definiert werden, als die oben erwähnten Datenstandards und Schnittstellenspezifikationen.

Auch können Datenstrukturen in hierarchischen Beziehungen ausgehend von einer Stammstruktur oder Kerndatenmodellen eingeordnet sein.
Darüber hinaus kann es auch Abhängigkeiten oder Beziehungen zwischen verschiedenen Informationsobjekten auch über Fachdomänen hinweg geben.
