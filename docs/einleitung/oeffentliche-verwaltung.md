---
sidebar_position: 2
description: Öffentliche Verwaltung und Informatisierung kurz erklärt
---

# Die öffentliche Verwaltung als unbekanntes Wesen

Das Bild, das Bürger:innen und Unternehmen häufig von der öffentlichen Verwaltung haben, lässt sich als unflexibel, langsam und starr sowie ggf. unfreundlich beschreiben.
Diese Wahrnehmungen sind geprägt vom Alltagserleben und -handeln, entspringen aber in weiten Teilen einem tiefsitzenden Missverständnis bzw. einer gewissen Unkenntnis über den öffentlichen Sektor und die öffentliche Verwaltung sowie deren Aufgaben.
Damit bleibt auch verborgen, was öffentliche Verwaltungen tagtäglich leisten.
Wahrgenommen wird nur, was aus Sicht von Bürger:innen und Unternehmen nicht funktioniert.
Im Folgenden wird überblicksartig versucht, die öffentliche Verwaltung sowie ihre Funktionsweise und Aufgaben zu definieren, um so ein besseres Verständnis zu vermitteln.

## Definition öffentliche Verwaltung
Eine allgemeingültige Definition der öffentlichen Verwaltung ist in der Literatur nicht zu finden.
Häufig wird hier öffentliche Verwaltung negativ bzw. über Abgrenzung definiert.
Danach ist die öffentliche Verwaltung Teil der ausführenden Gewalt (Exekutive), im Unterschied zum Parlament als Legislative und der Rechtssprechung (Judikative).
Der Begriff „die öffentliche Verwaltung“ suggeriert, dass es sich um ein homogenes Feld von Organisationen handelt, die im Wesentlichen primär öffentlich-rechtliche, bürokratisch-hierarchisch gestaltete Institutionen sind.
Dazu gehören bspw. Stadtverwaltungen und Landesministerien.
Daneben gibt es aber noch Institutionen mit privater Rechtsform, z.B. kommunale Wirtschaftsförderungen oder Versorger.

## Funktionsweise
### Ziel öffentlicher Verwaltungen
Grundsätzliches Ziel öffentlicher Verwaltungen ist es, öffentliche Aufgaben zu erfüllen, im Unterschied zu privatwirtschaftlichen Unternehmen, die Umsatz und Profit erzielen müssen.
Öffentliche Aufgaben entstehen grundsätzlich aus politischen Zielen, die bspw. in einem Wahlprogramm einer Partei formuliert werden und entstehen, weil ein öffentliches Interesse besteht.
Politische Ziele können dabei durchaus zueinander in Konflikt stehen.
Welche öffentlichen Aufgaben umzusetzen sind, wird in rechtlichen Regelungen niedergelegt, die häufig auch Bestimmungen enthalten, wie diese zu vollziehen sind.
Daraus ergibt sich eine Definition von öffentlicher Verwaltung im engeren Sinne, dass öffentliche Verwaltung jedes administrative Handeln ist, das dem Vollzug von rechtlichen Regelungen dient.
Diese Definition weist bereits darauf hin, dass die öffentliche Verwaltung besser über ihre Tätigkeiten definiert werden sollte.
Die Haupttätigkeiten der Verwaltung sind beobachten, entscheiden, intervenieren und kommunizieren [@StaatamDraht][@BWLöffentlicheVerwaltung] (auch im Folgenden).
Aus diesen Grundtätigkeiten resultieren als „Produkte“ der Verwaltung die Kommunikation von Entscheidungen als wichtigste Form der Intervention, v.a. gegenüber Bürger:innen und Unternehmen.
Weitere Produkte sind finanzielle Transfers wie Sozialleistungen und Subventionen sowie personenbezogene Leistungen, z.B. Pflege, Unterricht und Beratung.
Öffentliche Verwaltungen sorgen aber auch für die Schaffung von Infrastrukturen, z.B. Straßen, und deren Unterhalt.
Nicht zuletzt gibt die öffentliche Verwaltung Informationen ab, z.B. in Form von Warnungen vor Gefahren.
Auch wenn viele dieser Produkte nicht von der öffentlichen Verwaltung erbracht werden, sondern dafür private Dienstleister beauftragt werden, verbleibt die Verantwortung für die Erbringung dieser öffentlichen Aufgaben bei der öffentlichen Verwaltung.

### Handlungsgrundlage 
Das Handeln in einzelnen Verwaltungen ist durch die grundgesetzlich verankerten Prinzipien der Ressorthoheit (für die Ministerien) und der kommunalen Selbstverwaltung geprägt.
Beide Prinzipien räumen den jeweiligen Verwaltungen eine große Autonomie hinsichtlich ihrer Organisation, dem Umgang mit Finanzen usw. ein.
Diese Prinzipien haben u.a. aus demokratietheoretischer Perspektive und Gewaltenteilungssicht ihre Berechtigung.
Bislang wurden diese Prinzipien auch auf die IT ausgeweitet, was aber die Umsetzung von Digitalisierung im Sinne einer vernetzten Leistungserstellung erschwert.
Ebenso ist eine Kooperation zwischen diesen „autonomen“ Einheiten nicht eingeübt und im Grundsatz auch nicht vorgesehen [@Stein-Hardenberg2.0].
Dies betrifft auch den Datenaustausch.
Intern basiert die Arbeit der öffentlichen Verwaltungen auf Regelgebundenheit, Aktenmäßigkeit und Schriftlichkeit, was zusammen mit Arbeitsteilung und Spezialisierung die rechtsstaatliche und demokratische Kontrolle und Führung beim Verwaltungshandeln sichern soll [@VerwaltungundVerwaltungswissenschaften].
Diese Merkmale sollen pointiert formuliert die Gleichbehandlung gewährleisten.
Dadurch wirken die Verwaltungen jedoch auf Bürger:innen und Unternehmen als unzugänglich und unflexibel.
Für viele Akteure in der Verwaltung sind diese Prinzipien mit der Digitalisierung nicht vereinbar.
Die Forschung hat aber gezeigt, dass die Prinzipien im Grundsatz weiter fortbestehen können [@Stein-Hardenberg2.0].

## Zur Verwaltungsinformatisierung
Die geläufige Meinung, Verwaltungsmodernisierung, -digitalisierung oder -informatisierung seien Spätzünder oder neuartige Phänomene, ist ein Irrglaube.
Tatsächlich wird die Arbeit im öffentlichen Sektor bereits seit Mitte der 1950er Jahre, spätestens jedoch seit den 1970er Jahren durch informationstechnologische Fortschritte und damit einhergehenden Leitbildern verändert [@Informatisierung].
Beispiele hierfür sind die integrierte Datenhaltung mittels Datenbanken (70er), eine intensivierte Interaktion zwischen Mensch und Technik in Form von Personal Computern (Schreiben, Kalkulieren usw., 80er) und später das Internet sowie eine zunehmende Vernetzung (Mitte der 90er).
Zur Verdeutlichung nennt Schuppan Zahlen: 200 Fachverfahren, 150000 vernetzte PCs und 10000 Server (Stand 2011) [@Informatisierung].
Trotz dieser Entwicklung bleibt die Anerkennung dafür, aufgrund erhöhter Erwartungshaltungen an Reformvorhaben, aus.
Zudem wurden diese Erwartungen von Unwissenheit, „kommerziellen Interessen oder unreflektiertem Reformeifer“ genährt [@Informatisierung].
IT und Verwaltung können demnach nicht mehr getrennt voneinander gedacht werden.
Dennoch oder gerade deshalb kann weder von der IT oder der Verwaltung gesprochen werden.
Stattdessen ist eine gleichzeitig differenzierte wie integrierende Auseinandersetzung mit der Frage nach den jeweils eingesetzten Anwendungen und der Art und Weise des Einsatzes dieser notwendig.
Ziel ist es also, eine Vielzahl an technischen Möglichkeiten inklusive der verschiedenen Nutzungsarten und -potenziale einer einzelnen Anwendung mit dem Facettenreichtum von Veraltungsarbeit zusammenzudenken, zumal diese nahezu durch und durch als eine wie auch immer geartete Informationsarbeit beschrieben werden kann [@Informatisierung].
Die praktische Umsetzung dieser Idee wird zusätzlich von Marktinteressen, welche die Verwaltungslogik außer Acht lassen, sowie durch „politische, kulturelle, ökonomische Rahmenbedingungen“ begleitet [@Informatisierung].
Weiterhin müssen diese in bereits bestehende rechtliche und technische Strukturen eingefügt werden.
Zuletzt hängen Informatisierungsvorhaben ebenso von verfügbaren Ressourcen und formulierten Strategien ab [@Informatisierung].
Aufgrund der daraus ableitbaren Wechselwirkungen bedarf es aktiver Gestaltungsansätze und dem Willen, Digitalisierung in Form einer Gesamtarchitektur aus IT, Organisation und übergreifender Vernetzung zu denken und das Potenzial von IT im Organisationskontext zu erfassen [@Informatisierung].
Dies impliziert, sich an Verfahrensabläufen zu orientieren, um daraus neue Möglichkeiten der Organisations- und Prozessgestaltung abzuleiten und diese umzusetzen [@Informatisierung].
Beispiele hierfür können digitale Governance-Strukturen sein sowie eine verbesserte Nachvollziehbarkeit von Prozessen, neue Kooperationsbeziehungen durch digitale Kommunikations- und Interaktionswerkzeuge oder gar die Modularisierung von Abläufen, wodurch einzelne Schritte arrangiert werden können [@Informatisierung].

Auf Basis dieser Potenziale rückt das Ziel einer vernetzten, ressortübergreifenden Verwaltung und damit einhergehenden Effekten der Effizienzsteigerung und Kostensenkung immer weiter in den Fokus der bundesdeutschen Digitalisierungspolitik, wie zu einem späteren Zeitpunkt (Kap. zu Rahmenbedingungen) erläutert wird.
Zur Steigerung der Erfolgschancen bei der Umsetzung bedarf es jedoch eines grundlegenden Verständnisses des föderalen Prinzips der Bundesrepublik, das Mehrebenensystem, welches im Folgenden kurz vermittelt werden soll.

## Aufbau öffentliche Verwaltung
### Mehrebenensystem
Wie bereits geschildert, lässt sich Verwaltung als ein vielschichtiges und breitgefächertes Gebilde begreifen, welches ein ebenso umfangreiches Aufgabenspektrum umfasst.
Dieses Spektrum bildet zugleich die Basis ihrer horizontalen wie vertikalen Ausdifferenzierung in Ebenen, Ressorts und Organisationseinheiten.
Zusätzlich werden die Zuständigkeiten von Verwaltung durch territorial gesetzte Einheiten definiert [@Mehrebenensystem].
Die Verwaltungswissenschaften sprechen daher von Mehrebenensystemen, welche sowohl durch Kooperationsbeziehungen verschiedener Verwaltungsebenen (Bsp. EU), als auch durch eine Dezentralisierung der Umsetzung von Recht definiert sind – oftmals in unterschiedlich gearteten Mischformen.

Im bundesdeutschen Kontext geht die Gesetzgebung weitestgehend vom Bund aus, während die Länder meist die Verantwortung für deren Ausführung innehaben.
Dennoch hat auch der Bund eigene Verwaltungsaufgaben zu erfüllen, darunter in der gesetzesvorbereitenden Ministerialverwaltung.
Hier gilt das föderalistische Prinzip, dass jede Ebene eine eigene Verwaltung hat.
Die Regel Bund gleich Gesetz und Land/Kommune gleich Vollzug ist demnach nicht allgemeingültig.
Stattdessen müssen Verwaltungsaufgaben von Bund, Ländern und Kommunen in Wechselwirkung und in Form komplexer und komplementärer Hierarchiegebilde betrachtet werden [@Mehrebenensystem].
Erstens gibt die vorwiegend alleinige Vollzugshoheit der Länder und Kommunen diesen einen gewissen Handlungsspielraum bei der Umsetzung von Gesetzen und somit Handlungsmacht gegenüber der Gesetzgebung.
Zweitens gibt es mehrere Fälle, bei denen die Aufgaben der Bundesverwaltung eng mit dem Zuständigkeitsbereich von Landes- und Kommunalverwaltung zusammenhängen, wodurch eine Kooperation und notwendigerweise Koordination im/des Verwaltungshandeln der unterschiedlichen Ebenen nötig wird.
Als Beispiel kann hier die Arbeitsmarktverwaltung genannt werden (für eine nähere Beschreibung des Beispiels siehe ebd. 91 f.) [@Mehrebenensystem].
Auch die Steuerung der Umsetzung von Bundesgesetzen auf Landes-/Kommunalebene erfolgt in der Regel in Form von Kooperation und Kommunikation [@Mehrebenensystem].
Zuletzt nehmen Länder und Kommunen auch direkt Einfluss auf die Bundesgesetzgebung, indem diese im Rahmen zahlreicher Gremien Erfahrungen und Interessen in die Gesetzesvorbereitung einfließen lassen können.
Für die Erfüllung von ebenenübergreifenden Gemeinschaftsaufgaben wurde unterstützend Art. 91a-e GG erlassen [@Mehrebenensystem].

Die beschriebene Territorial- und Aufgabentrennung wird zudem rechtlich durch das Bundesstaatlichkeitsprinzip unterstützt, wodurch „gemäß Art 83 GG der Vollzug der Bundesgesetze grundsätzlich eine Angelegenheit der Länder“ ist (kursiv im Original s.o.) [@Rahmenbedingungen].
Art 84 Abs. 1 S. 7 und Art 85 Abs. 1 S. 2 GG besagen, dass der Bund den Kommunen keine Aufgaben zuschreiben darf.
Dies ist, gesetzlich geregelt, Sache der Länder, wodurch auch der oben genannte Handlungsspielraum offen bleibt [@Rahmenbedingungen].
Zudem sind die Länder für das Kommunalrecht verantwortlich, wodurch Verwaltungsreformen meist auf Länderebene durchgeführt werden [@Rahmenbedingungen].
Die oben genannten komplexen Hierarchiesysteme werden umfassend durch verschiedene Staatsstruktur- und weitere Prinzipien rechtlich gerahmt, durch das Haushaltsrecht, das Demokratieprinzip, das Prinzip der kommunalen Selbstverwaltung usw. [@Rahmenbedingungen].
Diese werden in diesem Kontext jedoch aus Gründen der Komplexitätsreduktion und der Relevanz lediglich genannt und können detailliert in Mehde (2019) nachgeschlagen werden.
Zuletzt soll das bereits oben genannte Ressortprinzip genannt werden, welches für die Organisationsgestaltung und somit für die Informatisierung relevant ist [@Rahmenbedingungen].

Entsprechend dieser Ausführungen müssen Reformvorhaben, wie eine Informatisierung der Verwaltung, bundesweit oder lediglich einen Bereich betreffend, immer im Kontext des komplexen Mehrebenensystems betrachtet werden – aus rechtlicher, finanzieller, koordinativer oder organisationeller Perspektive.

## Quellen
