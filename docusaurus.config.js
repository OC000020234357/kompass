/** @type {import('@docusaurus/types').DocusaurusConfig} */
const gitBranch = process.env.GIT_BRANCH || 'main'

const baseUrl = process.env.DOCUSAURUS_BASE_URL || '/kompass/'

async function createConfig() {
  const rehypeCitation = (await import('rehype-citation')).default;
  return {
    title: 'Kompass der föderalen IT-Architektur',
    url: 'https://docs.fitko.de',
    baseUrl,
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'throw',
    favicon: 'favicon.png',
    i18n: {
      defaultLocale: 'de',
      locales: ['de'],
    },
    themeConfig: {
      docs: {
        sidebar: {
          hideable: true,
        },
      },
      colorMode: {
        disableSwitch: true,
      },
      navbar: {
        title: 'Kompass der föderalen IT-Architektur',
        items: [
          {
            type: 'html',
            value: `<a href="/"><img src="${baseUrl}assets/icons/chevron-left.svg" alt="Zurück" /></a>`,
            position: 'left',
            className: 'fitko-navbar-icon--back',
          },
        ],
      },
      footer: {
        style: 'light',
        copyright: `Diese Seite ist Teil des <a href="https://docs.fitko.de/">Föderalen Entwicklungsportals</a>. Verantwortlich für die Inhalte der Seite sind die jeweiligen Autoren. Wenn nicht anders vermerkt, sind die Inhalte dieser Webseite lizenziert unter der <a href="https://creativecommons.org/licenses/by/4.0/deed.de">Creative Commons Namensnennung 4.0 International Public License (CC BY 4.0)</a>. Die technische Infrastruktur wird betrieben durch die FITKO. Es gilt das <a href="https://www.fitko.de/impressum">Impressum der FITKO</a> und die <a href="https://fitko.de/datenschutz">Datenschutzerklärung der FITKO</a> mit der Maßgabe, dass kein Tracking durchgeführt wird und keine Cookies gesetzt werden.`,
      },
    },
    plugins: [
      require.resolve("@cmfcmf/docusaurus-search-local"),
    ],
    presets: [
      [
        '@docusaurus/preset-classic',
        {
          docs: {
            sidebarPath: require.resolve('./sidebar.js'),
            editUrl: ({ version, versionDocsDirPath, docPath }) =>
              `https://gitlab.opencode.de/opendva/kompass/edit/${gitBranch}/${versionDocsDirPath}/${docPath}`,
            routeBasePath: 'docs',
            breadcrumbs: false,
            showLastUpdateTime: true,
            rehypePlugins: [
              [rehypeCitation,
                {
                  path: "./citation_config",
                  bibliography: "references.bib",
                  csl: "din-1505-2-numeric.csl",
                  lang: "csl-de-DE.xml",
                  linkCitations: true
                }],
            ],
          },
          theme: {
            customCss: require.resolve('./src/css/custom.css'),
          },
        },
      ],
    ],
    scripts: [{
      src: `${baseUrl}js/custom.js`,
      async: false,
    }],
    markdown: {
      mermaid: true,
    },
    themes: ['@docusaurus/theme-mermaid'],
  };
}

module.exports = createConfig;
